import React from "react";
import {v4 as uuidv4} from 'uuid';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppMasterComponent from "./AppMasterComponent";

function NavbarTop (props){
  const {titleText, author} = props;
  return(
    <div>
        <h1 className="appTitleStyle">{titleText}</h1>
        <h4 className="appAuthorStyle">{author}</h4>
    </div>
  );
}

function App() {
  const titleText = "Welcome To React Basic App";
  const author = "Nazmulcs42";

  return(
    <div>
      <NavbarTop titleText = {titleText} author={author} />
      <AppMasterComponent />
    </div>
  );
}
export default App;

