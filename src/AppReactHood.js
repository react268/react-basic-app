import React from "react";
import {v4 as uuidv4} from 'uuid';

function NavbarTop (props){
  const {titleText, author} = props;
  return(
    <div>
        <h1 className="appTitleStyle">{titleText}</h1>
        <h4 className="appAuthorStyle">{author}</h4>
    </div>
  );
}

function Welcome1(){

  return React.createElement("h1",{},"Welcome");
}
function Welcome2(){

  return <h1> Welcome</h1>;
}

function Todo1(){
   
  return React.createElement(
    "div",
    {},
    React.createElement("p",{},"Todo title1"),
    React.createElement("p",{},"Todo desc1")
    );
}

function Todo2(){
  return <div>
    <p>Todo title2</p>
    <p>Todo desc2</p>
  </div>
}

function App() {
  const titleText = "Welcome To React Basic App";
  const author = "Nazmulcs42";

  return(
    <>
      <NavbarTop titleText = {titleText} author={author}/>
     <Welcome1 />
     <Welcome2 />
     <Todo1 />
     <Todo2 />
    </>
      
  );
}
export default App;

