import React, { Component } from 'react'
import ReactEventHandling from './components/ReactEventHandling'

export default class AppEventHandler extends Component {
  render() {
    return (
      <div>
        <ReactEventHandling />
      </div>
    )
  }
}
