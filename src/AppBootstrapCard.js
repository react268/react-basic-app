import React from "react";
import {v4 as uuidv4} from 'uuid';
import BootstrapCard from "./components/BootstrapCard";
import 'bootstrap/dist/css/bootstrap.min.css';

function NavbarTop (props){
  const {titleText, author} = props;
  return(
    <div>
        <h1 className="appTitleStyle">{titleText}</h1>
        <h4 className="appAuthorStyle">{author}</h4>
    </div>
  );
}

function App() {
  const titleText = "Welcome To React Basic App";
  const author = "Nazmulcs42";

  return(
    <div>
      <NavbarTop titleText = {titleText} author={author}/>
      <BootstrapCard  cardPhoto = "images/tom-cruise.jpg" cardTitle="Tom Cruise" cardText="Hi everyone! I am a hollywood actor from United Nation of America. Presently, searching a dynamic position for expressing my acting power. Thanks for the support." />
      <BootstrapCard  cardPhoto = "images/boy.png" cardTitle="Clasic Man" cardText="Hi everyone! I am a hollywood actor from United Nation of America. Presently, searching a dynamic position for expressing my acting power. Thanks for the support." />
      <BootstrapCard  cardPhoto = "images/hlw_2022.jpg" cardTitle="Nazmul Biswas" cardText="Hi everyone! I am a hollywood actor from United Nation of America. Presently, searching a dynamic position for expressing my acting power. Thanks for the support." />
      <BootstrapCard  cardPhoto = "images/man.jpg" cardTitle="Hridoy Patuarry" cardText="Hi everyone! I am a hollywood actor from United Nation of America. Presently, searching a dynamic position for expressing my acting power. Thanks for the support." />
      <BootstrapCard  cardPhoto = "images/mmm.png" cardTitle="Boris Jonson" cardText="Hi everyone! I am a hollywood actor from United Nation of America. Presently, searching a dynamic position for expressing my acting power. Thanks for the support." />
      <BootstrapCard  cardPhoto = "images/women.jpeg" cardTitle="Fatima Khatun" cardText="Hi everyone! I am a hollywood actor from United Nation of America. Presently, searching a dynamic position for expressing my acting power. Thanks for the support." />
      <BootstrapCard  cardPhoto = "images/face_PNG5660.png" cardTitle="Aminul Haque" cardText="Hi everyone! I am a hollywood actor from United Nation of America. Presently, searching a dynamic position for expressing my acting power. Thanks for the support." />

    </div>
      
  );
}
export default App;

