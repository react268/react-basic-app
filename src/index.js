import React from 'react';
import ReactDOM from 'react-dom/client';
import AppFunctionalComponent from './AppFunctionalComponent';
import AppClassComponent from './AppClassComponent';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <>
    <App />
    {/* <AppFunctionalComponent /> */}
    {/*  <AppClassComponent /> */}
  </>
);
