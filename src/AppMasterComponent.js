import React from 'react'
import { ToggleButton } from 'react-bootstrap';
import AppEventHandler from './AppEventHandler';
import AppReactConditionalRender from './AppReactConditionalRender';
import AppReactHooks from './AppReactHooks';
import BootstrapAlertwithState from './components/BootstrapAlertwithState';
import CustomUseEffectExample from './components/customHooks/fetchData/CustomUseEffectExample';
import Users from './components/hooks/Users';
import ReactBootstrapAlertwithState from './components/ReactBootstrapAlertwithState';
import ReactBootstrapContrainer from './components/ReactBootstrapContrainer';
import ReactState from './components/ReactState';
import Toggle from './components/toggle/Toggle';

const AppMasterComponent = () => {

  return (
    <div>
      {/* <ReactBootstrapAlertwithState />
      <BootstrapAlertwithState /> 
      <ReactBootstrapContrainer />*/}
      {/* <ReactState /> */}
      {/* <AppReactConditionalRender /> */}
      {/* <AppEventHandler /> */}
     {/*  <AppReactHooks /> */}
     {/* <Users/> */}
     {/* <Toggle /> */}
     <CustomUseEffectExample/>
      
    </div>
  )
}

export default AppMasterComponent;