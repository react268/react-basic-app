import React from "react";
import Card from "./components/Card";
import Data from './jsonData/data';
import Persons from './jsonData/nestedData';
import {v4 as uuidv4} from 'uuid';
//import logo from './logo.svg';
//import './App.css';


//import './style.css';
/* 
const appTitleStyle = {
  color : "red",
  fontSize : "28.5px",
  backgroundColor : "purple",
  textAlign : "center",
  padding : "15px"
}
const appAuthorStyle = {
  color : "blue"
}
const appDescStyle = {
  color : "#000"
}
const appDateStyle = {
  color : "gray",
  fontStyle : "italic",
  font : "squirrel"
} */
/* function AppInfo(props){
  const {appTitle, appAuthor, appDesc} = props;
  //console.log('AppInfo-props: ');
  //console.log(props);
  return (
    <div>
      <h1 className="appTitleStyle">{appTitle}</h1>
      <h4 className="appAuthorStyle">{appAuthor}</h4>
      <h5 className="appDateStyle">{new Date().toLocaleDateString()}</h5>
      <p  className="appDescStyle">{appDesc}</p>
    </div>
  );
} */
function NavbarTop (props){
  const {titleText, author} = props;
  return(
    <div>
        <h1 className="appTitleStyle">{titleText}</h1>
        <h4 className="appAuthorStyle">{author}</h4>
    </div>
  );
}

function App() {
  //console.log(Data);
  const titleText = "Welcome To React Basic App";
  const author = "Nazmulcs42";
  /* let cardItems = [];
  cardItems = Data.map((cardItem,index) => <Card key={index} cardTitle={cardItem.title} cardMessage={cardItem.desc}/>)
 */

  //persons -> nested data
 // console.log(Persons);
  //console.log(Persons[0].address[1].present);
  return(
    <>
      <NavbarTop titleText = {titleText} author={author}/>
      {/* {cardItems} */}
      {/* {Data.map((cardItem,index) => <Card key={index} cardTitle={cardItem.title} cardMessage={cardItem.desc}/>)} */}
      {/* {Data.map(function(cardItem){
        //console.log(uuidv4());
        return <Card key={uuidv4()} cardTitle={cardItem.title} cardMessage={cardItem.desc}/>
      })}; */}
      {
        Persons.map((person, index) => {
          return <article key={index}>
            <h3>Full Name : {person.fullName}</h3>
            <p>Age  : {person.age}</p>
            <p>Phone : {person.phone}</p>
            {
              person.address.map( (addr,index) => {
                console.log(addr)
               return <div key={index}>
                  <p>Present Address : {addr.present}</p> 
                  <p>Permanent Address : {addr.permanent}</p>
                </div>
              })
            }
          </article>
        })
      };

     {/*  {Persons.map((person, index) => {
       
        const desc ="Mr. "+person.fullName+" is "+person.age+" years old. His present address is "+person.address[1].present+" You can make a phone call by "+person.phone+". You may also communicate with him by his permanent address of "+person.address[0].permanent+" Thanks";
       
        return <Card key={index} cardTitle={person.fullName} cardMessage={desc}/>;
      })}; */}
    </>
      
  );

 /*  const titleText = "Welcome To React Basic App";
  const author = "Nazmulcs42";

  let cardItems = [];
  for(let i=0; i<Data.length; i++){
    cardItems.push(
      <Card cardTitle={Data[i].title} cardMessage={Data[i].desc}/>
    );
  }

  return(
    <div>
      <NavbarTop titleText = {titleText} author={author}/>
      {cardItems}
    </div>
      
  );
 */
/* 
const appTitle = "Welcome To React Basic App";
const appAuthor = "Nazmulcs42";
const appDesc = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc.";

  return (
    <div>
    <AppInfo appTitle={appTitle} appAuthor={appAuthor} appDesc = {appDesc}/>
    <br/>
    <br/>

    <Card cardTitle="Aamra Networks Limited." cardMessage="With a platform that supports a cloud-native approach, enterprises build applications that run on any (public or private) cloud without modification."/>
    <Card cardTitle="Softzino Software Ltd." cardMessage="With a platform that supports a cloud-native approach, enterprises build applications that run on any (public or private) cloud without modification."/>
    <Card cardTitle="CloudlyIO Inc." cardMessage="With a platform that supports a cloud-native approach, enterprises build applications that run on any (public or private) cloud without modification."/>
    <Card cardTitle="Logic Software Ltd." cardMessage="With a platform that supports a cloud-native approach, enterprises build applications that run on any (public or private) cloud without modification."/>
    <Card cardTitle="ACI Group." cardMessage="With a platform that supports a cloud-native approach, enterprises build applications that run on any (public or private) cloud without modification."/>

  </div>
  ); */
}

export default App;





    {/* <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div> */}