import React, {useState } from 'react'
import { ReactConditionalRenderHomePage } from './components/ReactConditionalRenderHomePage'
import { ReactConditionalRenderLoginPage } from './components/ReactConditionalRenderLoginPage'

export default function AppReactConditionalRender (){
    const {isLoggedIn, setisLoggedIn} = useState(false);
    console.log(isLoggedIn);
    return (
        <div>
          {isLoggedIn && <ReactConditionalRenderHomePage />}
          {!isLoggedIn && <ReactConditionalRenderLoginPage />}
        </div>
    )
}

/* 
export default class AppReactConditionalRender extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       isLoggedIn : false
    }
  }
    render() {
        const {isLoggedIn}  = this.state ; 
        let element;
        if(isLoggedIn){
            element = <ReactConditionalRenderHomePage />
        }
        else{
            element = <ReactConditionalRenderLoginPage />
        }
        return <div> {element} </div>;
        
        if(this.state.isLoggedIn){
            return(
                <ReactConditionalRenderHomePage />
            );
        }
        else{
            return (
            <ReactConditionalRenderLoginPage />
            );
        } 
  }
}

 */


/* 
export default class AppReactConditionalRender extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         isLoggedIn : false
      }
    }
      render() {
  
      return (
        <div>
          {this.state.isLoggedIn && <ReactConditionalRenderHomePage />}
          {!this.state.isLoggedIn && <ReactConditionalRenderLoginPage />}
        </div>
      )
    }
  } */
