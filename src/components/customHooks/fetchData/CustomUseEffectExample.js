import React from 'react'
import useFetchData from './useFetchData';

const CustomUseEffectExample = () => {
    const {data, error, loading} = useFetchData("https://jsonplaceholder.typicode.com/todos");

  return (
    <div>
        <h1>Data from Api (Custom useEffectHook)</h1>
        {loading && <p>Data is loading...</p>}
        {error && <p>{error}</p>}
        {
            data &&
            data.map((record, index) => {
                return <p key={index}> {record.title}</p>
            })
        }
    </div>
  )
}

export default CustomUseEffectExample