import React, { useEffect, useState } from 'react';


const useFetchData = (url) => {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {

    fetch(url)
    .then((res) =>{
        if(!res.ok){
            throw Error ('Fetching is not successfull...');
        }
        return res.json();
    })
    .then((data)=>{
        setData(data);
        setError(null)
        setLoading(false)
    })
    .catch((err)=>{ 
        setData(null);
        setError(err.message)
        setLoading(false)
    })

  },[url])

  return {data, error, loading};
}

export default useFetchData;