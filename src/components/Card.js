

function Card (props) {
    const {cardTitle, cardMessage} = props;
   // console.log('Card-props: ');
    //console.log(props);
    return(
      <div className='card'>
        <div className='cardTitle'>
         {cardTitle}
        </div>
        <div className='cardBody'>
          <div className='cardMessage'>
            {cardMessage}
          </div>
          <div className='cardFooter'>
            {new Date().toLocaleDateString()}
          </div>
        </div>
      </div>
    );
  }

  export default Card;