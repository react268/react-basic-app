import React from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

const BootstrapCard = (props) => {
  const {cardPhoto, cardTitle, cardText} = props;
  return (
    <Card style={{ width: '18.5rem', display:"inline-block" }}>
      <Card.Img variant="top" src={cardPhoto} width="100%" height="170px" />
      <Card.Body style={{textAlign : "center"}} className="px-0 mx-0">
        <Card.Title>{cardTitle}</Card.Title>
        <Card.Text style={{textAlign : "justify"}}>{cardText}</Card.Text>
        <Button variant="primary" >Get me more</Button>
      </Card.Body>
    </Card>
  )
}

export default BootstrapCard;