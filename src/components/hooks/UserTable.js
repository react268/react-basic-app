import React from 'react'
import { Button, Table } from 'react-bootstrap'

const UserTable = (props) => {

    const deleteUser = (userID) => {
        props.onDeleteData(userID);
    }

    const editUser = (userID) => {
        props.onEditData(userID);
    }

  return (
    <div>
        <Table responsive striped hover bordered size='sm'>
        <thead>
            <tr className='primary'>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th colSpan={2}>Full Name</th>
                <th className='text-center'>Action</th>
            </tr>
        </thead>
        <tbody>
            {
                props.users &&
                props.users.map((user, index) => {
                    return  <tr key={index}>
                        <td>{(index+1).toString().padStart(2, '0')}</td>
                        <td>{user.firstName}</td>
                        <td>{user.lastName}</td>
                        <td colSpan={2}>{user.firstName} {user.lastName}</td>
                        <td className='text-center'>
                            <Button variant='primary' size='sm' onClick={() => editUser(user.id)} >Edit</Button>
                            <Button variant='danger' size='sm' onClick={() => deleteUser(user.id)}>Delete</Button>
                        </td>
                    </tr>;
                })
            }
           
        </tbody>
    </Table>
    </div>
  )
}

export default UserTable