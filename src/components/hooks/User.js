import React from 'react'
import { Table } from 'react-bootstrap';

const User = (props) => {
  const {firstName, lastName} = props.user;
  return (
    
    <Table responsive striped hover bordered size='sm'>
        <thead>
            <tr className='primary'>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th colSpan={2}>Full Name</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>01</td>
                <td>{firstName}</td>
                <td>{lastName}</td>
                <td>{firstName} {lastName}</td>
            </tr>
        </tbody>
    </Table>
  )
}

export default User