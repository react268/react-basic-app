import React, { useState } from 'react'
import { Button, Col, Container, Form, Row, Stack } from 'react-bootstrap'
import {v4 as uuidv4} from 'uuid'


const NewUser = (props) => {
    const [user, setUser] = useState({
        id : "",
        firstName : "",
        lastName : ""
    });

    const handleOnChange = (e) =>{
        setUser({...user, id : uuidv4(), [e.target.name] : e.target.value})
    }

    const onSubmitt = (e) =>{
        e.preventDefault();
        props.onNewData(user);
        setUser({
            id : "",
            firstName : "",
            lastName : ""
        })
        e.stopPropagation();
    }

    return (
    
        <Container fluid>
            <Form onSubmit={onSubmitt}>
                <Stack gap={3}>
                    <Row xs={12}> 
                        <Col sm={12} md={8} lg={7}  className="pb-3">
                            <Form.Group >
                                <Form.Label> First Name <span className="text-danger">*</span></Form.Label>
                                <Form.Control type='text' size='sm' name='firstName' value={user.firstName} placeholder='Enter your first name' onChange={handleOnChange} required/>
                            </Form.Group>
                        </Col>
                        <Col sm={12} md={8} lg={7}  className="pb-3">
                            <Form.Group >
                                <Form.Label> Last Name <span className="text-danger">*</span></Form.Label>
                                <Form.Control type='text' size='sm' name='lastName' value={user.lastName} placeholder='Enter your last name' onChange={handleOnChange} required/>
                            </Form.Group>
                        </Col>
                        <Col sm={12} md={12} lg={12}  className="pb-3">
                            <Button variant='primary' type='submit' pill='true' >registration</Button>
                        </Col>
                    </Row>
                </Stack>
            </Form>
        </Container>
    )
}

export default NewUser