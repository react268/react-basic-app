import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


const ReactBootstrapContrainer = () => {
  return (
    
    <Container fluid="sm">
      <Row>
        <Col className="bg-primary bg-gradient">1 of 3</Col>
        <Col xs={6} className="bg-warning">2 of 3 (wider)</Col>
        <Col className="bg-success bg-gradient">3 of 3</Col>
      </Row>
      <Row>
        <Col className="bg-info bg-gradient">1 of 3</Col>
        <Col xs={5} className="bg-secondary">2 of 3 (wider)</Col>
        <Col className="bg-info bg-gradient">3 of 3</Col>
      </Row>
    </Container>
  )
}

export default ReactBootstrapContrainer;