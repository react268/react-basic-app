import React, { Component } from 'react'
import { Button, Col, Container, Form, Row, Stack, Table } from 'react-bootstrap'

export default class ReactEventHandling extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
            firstName : '',
            lastName : ''
      }
      
    }

    handleClick = () =>{
        console.log('Clicked on submission button.')
    }

    handleFirstNameChange = (e) =>{
        this.setState({
            firstName : e.target.value
        },() =>{
            console.log("First Name: "+this.state.firstName)
        })
    }

    handleLastNameChange = (e) =>{
        this.setState({
            lastName : e.target.value
        },() =>{
            console.log("Last Name: "+this.state.lastName)
        })
    }

    onSubmitt = (e) =>{
        e.preventDefault();
        console.log('this.state')
        console.log(this.state)
        e.stopPropagation();
    }

    render() {
    return (
      <div>
        <Container fluid>
            <Form onSubmit={this.onSubmitt}>
                <Stack gap={3}>
                    <Row xs={12}> 
                        <Col sm={12} md={8} lg={7}  className="pb-3">
                            <Form.Group >
                                <Form.Label> First Name <span className="text-danger">*</span></Form.Label>
                                <Form.Control type='text' size='sm' name='fname' placeholder='Enter your first name' onChange={this.handleFirstNameChange} required/>
                            </Form.Group>
                        </Col>
                        <Col sm={12} md={8} lg={7}  className="pb-3">
                            <Form.Group >
                                <Form.Label> Last Name <span className="text-danger">*</span></Form.Label>
                                <Form.Control type='text' size='sm' name='lname' placeholder='Enter your last name' onChange={this.handleLastNameChange} required/>
                            </Form.Group>
                        </Col>
                        <Col sm={12} md={12} lg={12}  className="pb-3">
                            <Button variant='secondary' type='submit' pill='true' onClick={this.handleClick}>Click here</Button>
                        </Col>
                    </Row>
                </Stack>
            </Form>

            <Table responsive striped hover bordered size='sm'>
                <thead>
                    <tr className='primary'>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th colSpan={2}>Full Name</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>01</td>
                        <td>Nazmul</td>
                        <td>Biswas</td>
                        <td>Nazmul Biswas</td>
                    </tr>
                    <tr>
                        <td>02</td>
                        <td>Aminul</td>
                        <td>Islam</td>
                        <td>Aminul Islam</td>
                    </tr>
                    <tr>
                        <td>03</td>
                        <td>Ashraful</td>
                        <td>Islam</td>
                        <td>Ashraful Islam</td>
                    </tr>
                    <tr>
                        <td>04</td>
                        <td>Shabuj</td>
                        <td>Hossain</td>
                        <td>Shabuj Hossain</td>
                    </tr>
                </tbody>
            </Table>
        </Container>
      </div>
    )
  }
}
