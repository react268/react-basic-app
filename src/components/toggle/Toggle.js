import React, { useState } from 'react'
import { Button } from 'react-bootstrap'

const Toggle = () => {
  const [toggle, setToggle] = useState(true);
  return (
    <div className='container'>
        {
          toggle &&
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since the
            1500s, when an unknown printer took a galley of type and scrambled it to
            make a type specimen book. It has survived not only five centuries, but
            also the leap into electronic typesetting, remaining essentially
            unchanged. It was popularised in the 1960s with the release of Letraset
            sheets containing Lorem Ipsum passages, and more recently with desktop
            publishing software like Aldus PageMaker including versions of Lorem Ipsum.

          </p>
        }
        <Button  variant='primary' size='sm' onClick={() => setToggle(true)}>Show</Button>
        <Button  variant='danger' size='sm' onClick={() => setToggle(false)}>Hide</Button>
    </div>
  )
}

export default Toggle