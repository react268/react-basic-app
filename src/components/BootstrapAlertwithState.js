import React from 'react'

const BootstrapAlertwithState = () => {
  
  return (
    <div>
        <div class="alert alert-success alert-dismissible fade show firstCollapsible" role="alert">
        <strong>How's it going?!</strong>
        <p>
            Duis mollis, est non commodo luctus, nisi erat porttitor ligula,
            eget lacinia odio sem nec elit. Cras mattis consectetur purus sit
            amet fermentum.
        </p>
        <hr/>
        <div class="d-flex justify-content-end">
            <button type="button" class="btn btn-outline-success">Close me ya'll!</button>
        </div>
        </div>
        <div class="d-flex justify-content-start alert fade show">
            <button type="button" class="btn btn-primary d-none secondCollapsible">Show Alert</button>
        </div>

    </div>
  )
}

export default BootstrapAlertwithState;

// Javascript
/* $('.btn-outline-success').on('click', function(e) {
    $('.firstCollapsible').addClass('d-none');
    $('.secondCollapsible').removeClass('d-none');
})

$('.btn-primary').on('click', function(e) {
    $('.firstCollapsible').removeClass('d-none');
    $('.secondCollapsible').addClass('d-none');
}) */