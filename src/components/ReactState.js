import React, { Component } from 'react'
import { Button, Container, Row, Col } from 'react-bootstrap';

export default class ReactState extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         count : 0
      }
    }

    handleIncrement = () => {
        console.log("Count: " + this.state.count)
        this.setState({
            count: this.state.count + 1
        })
    }

    handleDecrement = () => {
        console.log("Count: " + this.state.count)
        this.setState({
            count: this.state.count - 1
        })
    }

  render() {
    const {count} = this.state;
    return (
      <div>
        <Container >
            <Row className="justify-content-left">
                <Col xs={4}>
                    <h1>count : {count}</h1>
                </Col>
                <Col xs={1} sm ="1" md="1" lg="1">
                    <Button variant="primary" size="lg" onClick={this.handleIncrement} >+</Button>
                </Col>
                <Col xs={1} sm ="1" md="1" lg="1">
                    <Button variant="danger" size="lg" onClick={this.handleDecrement} >-</Button>
                </Col>
            </Row>
            
        </Container>
      </div>
    )
  }
}
