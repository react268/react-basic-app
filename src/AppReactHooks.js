import React, { useState } from 'react'
import { Button, Col, Container, Form, Row, Stack, Table } from 'react-bootstrap'

const AppReactHooks = (props) => {

    //const [firstName, setFirstName] = useState("");
    //const [lastName, setLastName] = useState("");
    const [user, setUsers] = useState({
        firstName : "",
        lastName : ""
    });

    const handleFirstNameChange = (e) =>{
        setUsers({...user, firstName : e.target.value})
    }

    const handleLastNameChange = (e) =>{
        setUsers({...user, lastName : e.target.value})
    }

    const onSubmitt = (e) =>{
        e.preventDefault();
        props.onNewData(user);
        e.stopPropagation();
    }

    return (
    
        <Container fluid>
            <Form onSubmit={onSubmitt}>
                <Stack gap={3}>
                    <Row xs={12}> 
                        <Col sm={12} md={8} lg={7}  className="pb-3">
                            <Form.Group >
                                <Form.Label> First Name <span className="text-danger">*</span></Form.Label>
                                <Form.Control type='text' size='sm' name='fname' value={user.firstName} placeholder='Enter your first name' onChange={handleFirstNameChange} required/>
                            </Form.Group>
                        </Col>
                        <Col sm={12} md={8} lg={7}  className="pb-3">
                            <Form.Group >
                                <Form.Label> Last Name <span className="text-danger">*</span></Form.Label>
                                <Form.Control type='text' size='sm' name='lname' value={user.lastName} placeholder='Enter your last name' onChange={handleLastNameChange} required/>
                            </Form.Group>
                        </Col>
                        <Col sm={12} md={12} lg={12}  className="pb-3">
                            <Button variant='primary' type='submit' pill='true' >registration</Button>
                        </Col>
                    </Row>
                </Stack>
            </Form>
            <Table responsive striped hover bordered size='sm'>
                <thead>
                    <tr className='primary'>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th colSpan={2}>Full Name</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>01</td>
                        <td>{user.firstName}</td>
                        <td>{user.lastName}</td>
                        <td>{user.firstName} {user.lastName}</td>
                    </tr>
                </tbody>
            </Table>
        </Container>
    )
}

export default AppReactHooks



//#*********Class Component *********#

/* import React, { Component } from 'react'
import { Button, Container, Row } from 'react-bootstrap'

export default class AppReactHooks extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         count : 0
      }
    }
    
    handleIncrement = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    handleDecrement = () => {
        this.setState({
            count: this.state.count - 1
        })
    }

    render() {
        const {count} = this.state;
        return (
            <Container>
                <div>
                    <Row className='mb-3 text-center'>
                        <h2 className='text-primary'>Count : {count}</h2>
                    </Row>
                    <Row className='mb-3'>
                        <Button variant='outline-info' className='bg-gradient' size='sm' onClick={this.handleIncrement}>Increment</Button>
                    </Row>
                    <Row className='mb-3'>
                        <Button variant='outline-danger' className='bg-gradient' size='sm' onClick={this.handleDecrement}>Decrement</Button>
                    </Row>
                </div>
            </Container>
        );
  }
}
 */