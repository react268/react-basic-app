import React, {Component} from "react";
import {v4 as uuidv4} from 'uuid';


class NavbarTop extends Component {
  
  render(){
    return(
      <div>
          <h1 className="appTitleStyle">{this.props.titleText}</h1>
          <h4 className="appAuthorStyle">{this.props.author}</h4>
      </div>
    );
  }
}

class AppClassComponent extends Component {
  
  //const titleText = "Welcome To React Basic App";
  //const author = "Nazmulcs42";

  render(){
    return(
      <>
        <NavbarTop titleText = "Welcome To React Basic App" author="Nazmulcs42" />
  
      </>  
    );
  }
}

export default AppClassComponent;
